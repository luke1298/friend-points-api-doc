---
title: API Reference

language_tabs:
  - javascript (from Browser)

toc_footers:
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

Welcome to the Friend Points API! You can use our API to access Friend Points endpoints, which can get information on various users, groups, and points in our database.

We have language bindings in Shell, Ruby, Python, and Javascript! You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right.
In this documentation for internal simplicity we have provided examples using: npm\'s axios. You can find more on it [here](https://www.npmjs.com/package/axios).

This example API documentation page was created with [Slate](https://github.com/tripit/slate). Feel free to edit it and use it as a base for your own API's documentation.

# Authentication

> To authorize, use this code:

```javascript
axios.post('/api/v1/login/', {
  username: 'example@example.com',
  password: 'example123'
}).then(function(){
  //Insert whatever you want to do on successful login.
}).catch(function(){
  //Failed Login Reprompt.
});
```

Acceptable HTTP Methods: POST

If authentification is successful we'll return a 200 response and the users session id.

Else the endpoint responds with a 400.

<aside class="notice">
You must replace <code>username</code> and <code>password</code> with the users authentification data.
</aside>

# Groups

## Get All Groups

```javascript
axios.get('/api/v1/all_groups/', {
  userId: '1785960'
}).then(function(){
  //Insert whatever you want to do on successful group getting.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision
});
```

> The above command returns JSON structured like this:

```json
[
  {"groupId": "1894", "iconLoc": "<s3>", "isAdmin": true},
  {"groupId": "1489", "iconLoc": "<s3>", "isAdmin": false},
  {"groupId": "7876", "iconLoc": "<s3>", "isAdmin": true},
  {"groupId": "1924", "iconLoc": "<s3>", "isAdmin": true}
]
```

Acceptable HTTP Methods: GET

This endpoint retrieves all groups for the given userid, assuming that the current authenticated user has permssion to view that users groups.

If the request is successful the json list atriculated to the right (with appropriate data for that particular user id) will be returned (with a 200 status)

If the request is not successful then either a 404 (if the User id DNE) or a 401 (if the User requesting does not have appropriate credintials to request such data).


### HTTP Request

`GET http://example.com/api/v1/all_groups`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
includes | null | A list a atributes to include about each group. See `group` for a list of these atributes.
userID | null | (Required) The user id to get all groups for

## Get a Specific Group

```javascript
axios.get('/api/v1/group/', {
  groupId: '1894'
}).then(function(){
  //Insert whatever you want to do on successful getting of group.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision or the group doesn't exsist
});
```

> The above command returns JSON structured like this:

```json
{
  "id": 1894,
  "name": "Bradyn's House",
  "iconLoc": "s3.fjiopq.jb.com",
  "groupType": "Free For All",
  "points": [
    { "giver":"Sean M Wade", "receiver":"J Luke Nelson", "amount":10},
    { "giver":"J Luke Nelson", "receiver":"Bradyn Lee Bues", "amount":1},
    { "giver":null, "receiver":"Bradyn Lee Bues", "amount":1}
  ],
  "users": [
    { "name":"J Luke Nelson", "picture":"<s3>"},
    { "name":"Sean M Wade", "picture":"<s3>"},
    { "name":"Bradyn Lee Bues", "picture":"<s3>"}
  ]
}
```

Acceptable HTTP Methods: GET

This endpoint is used to get particular data about a specific group.

If correct credentials are provided (ie anyone in the group) then the api returns the appropriate data + a 200 status code
If the incorrect credentials are provided then a 401 status is returned.
If the group doesn't exsist then a 404.

<aside class="notice">
When "giver" is null then this implicitly implies that the points where assigned by the group itself.
</aside>

### HTTP Request

`GET http://example.com/api/v1/group/<ID>`

### URL Parameters

Parameter | Default | Description
--------- | ------- | -----------
ID || The ID of the kitten to retrieve
includePoints | true | When true the payload includes all the points
includeUsers | true | When true the payload includes all the users in the group and their relative data.


## Make a Group

```javascript
axios.post('/api/v1/group/', {
  userId: "18298",
}).then(function(){
  //Insert whatever you want to do on successful getting of group.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision or the group doesn't exsist
});

axios.put('/api/v1/group/', {
  groupId: '1894',
  add_member: "1230",
  add_admin: "123412",
  set_icon: "<s3>"
}).then(function(){
  //Insert whatever you want to do on successful creation of group.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision or the group doesn't exsist
});
```

Acceptable HTTP Methods: POST, PUT

This endpoint is used to create and modify groups.

The POST method will return 200 (given that the user has permissions to add groups) as well as a payload which includes the Group Id

The PUT method will return 200 as long as the "add_admin" consists of a user who is an admin (if this doesn't hold than it will return a 400).


## Delete a Group

```javascript
axios.post('/api/v1/delete_group/', {
  groupId: "1822",
}).then(function(){
  //Insert whatever you want to do on successful deletion of a group.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision or the group doesn't exsist
});
```

Acceptable HTTP Methods: POST

This endpoint is used to delete groups.

The POST method will return 200 if the group was properly "deleted" (which requires that the user have proper authentification).

If the proper authentification is not provided or the group id is not valid then we'll return a 400.

<aside class="notice">
We should never actually be deleting groups. It is to expensive to munge for all data that is dependent on the group. Instead we should have a flag on the groups tabel: "is_deleted"
</aside>

# Points

```javascript
axios.post('api/v1/give_points/', {
  "receiverId": "12584",
  "amount": 1,
  "message": null
}).then(function(){
  //Insert whatever you want to do on successful getting of group.
}).catch(function(){
  //Failed to Revtrive groups for the user doesn't have permision or the group doesn't exsist
});
```

Acceptable HTTP Methods: POST

This endpoint is used to give your friends points.

If the POST was successful then the receiverId will recieve your points. Unless their is some reason to prevent you from giving that point (like some sort of daily limit) then a 200 response will be returned.
Depending on why the error was thrown some 400 response will be returned.

